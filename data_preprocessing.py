"""
Preprocessing of the Crypto dataset
Converts the CSV files to Datatable .jay format and determines the earliest date for each coin
"""
#%% Setup
import datatable as dt
from datatable import f, sort
import pandas as pd

from _constants import *

#%% Conversion
start_days = []
for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
    for kline in TIMEFRAMES:
        # Load the dataset and convert timestamp
        df = pd.read_csv(f"data/{symbol}-{kline}-data.csv")
        df.loc[:, "timestamp"] = pd.DatetimeIndex(df["timestamp"]).astype(int)
        df = df.rename(columns={"timestamp": "Timestamp", "open": "Open", "high": "High", "low": "Low", "close": "Close", "volume": "Volume"})

        # Convert to data.table
        DT = dt.Frame(df[["Timestamp", "Open", "High", "Low", "Close", "Volume"]])

        # Store the earliest day
        if kline == "1d":
            start_days.append([symbol, DT[:, :, sort(f.Timestamp)][5, f.Timestamp].to_numpy()[0][0]])

        DT.to_jay(f"data/{symbol}-{kline}-data.jay")

# Convert the dates to a Datatable and save
dt.Frame(pd.DataFrame(start_days, columns=["Symbol", "Start Time"])).to_jay(f"data/start_dates.jay")