"""
Cryptocurrency Tools
A set of tools for fetching and manipulating cryptocurrency data
"""

# %%
# Dependencies

import os
import math

from datetime import timedelta, datetime
from dateutil import parser

#from bitmex import bitmex
from binance.client import Client

import numpy  as np
import pandas as pd

import mplfinance as mpf
import matplotlib.pyplot as plt

#%% Constants
API_KEY = "Ch8CWHQDC09M6AsXK1SOSQZNKPWLIONeqA0xNfEQLPDMbrcRHbhNtqoolpZlpOrx"
SECRET_KEY = "MSxYsuBX3iAWuczuljzJgDRRmA4hrIySPf1u8gv7bbfWKLTplGvjKlBEnkf3WrMu"
BIN_SIZES = {"1m":1, "5m":5, "15m": 15, "1h": 60, "6h":360,  "1d": 1440}

# %%
def minutes_of_new_data_(symbol, kline_size, data, source, client):
    """
    Fetches data from either BitMex or Binance for the desired pair
    """
    # Get existing data if it exists
    if len(data) > 0:
        old = parser.parse(data["timestamp"].iloc[-1])

    elif source == "binance":
        old = datetime.strptime("1 Jan 2017", "%d %b %Y")

    elif source == "bitmex":
        old = client.Trade.Trade_getBucketed(symbol=symbol
                                             , binSize=kline_size
                                             , count=1
                                             , reverse=False
                                             ).result()[0][0]["timestamp"]

    # Retrieve new data
    if source == "binance":
        new = pd.to_datetime(client.get_klines(symbol=symbol
                                               , interval=kline_size
                                               )[-1][0]
                             , unit="ms"
                             )

    if source == "bitmex":
        new = client.Trde.Trade_getBucketed(symbol=symbol
                                            , binSize=klines
                                            , count=1
                                            , reverse=True
                                            ).result()[0][0]["timestamp"]

    return old, new


# %%
def from_binance(symbol, kline_size, save=False):
    """
    Fetches data from the Binance API
    """
    # Create a binance
    client = Client(api_key=API_KEY, api_secret=SECRET_KEY)

    # Define a filename
    filename = f'''data/{symbol}-{kline_size}-data.csv'''

    # Load the file if it exists
    if os.path.isfile(filename):
        df = pd.read_csv(filename)
    else:
        df = pd.DataFrame()

    # Get the extrema timesteps
    oldest, newest = minutes_of_new_data_(symbol, kline_size, df, "binance", client)

    # Check the differenc in minutes and determine how much new data is available
    delta_minutes = (newest - oldest).total_seconds() / 60
    available = math.ceil(delta_minutes / BIN_SIZES[kline_size])

    # Print useful information
    if oldest == datetime.strptime("1 Jan 2017", "%d %b %Y"):
        print("Downloading all available %s data for %s" % (kline_size, symbol))
    else:
        print("Downloading %d minutes of new data available for %s" % (delta_minutes, symbol))

    # Get the number of lines of data to pull
    klines = client.get_historical_klines(symbol=symbol
                                          , interval=kline_size
                                          , start_str=oldest.strftime("%d %b %Y %H:%M:%S")
                                          , end_str=newest.strftime("%d %b %Y %H:%M:%S")
                                          )

    # Convert to DataFrame
    data = pd.DataFrame(klines
                        , columns=["timestamp"
            , "open"
            , "high"
            , "low"
            , "close"
            , "volume"
            , "close_time"
            , "quote_av"
            , "trades"
            , "tb_base_av"
            , "tb_quote_av"
            , "ignore"
                                   ]
                        )

    # Convert timestamp
    data["timestamp"] = pd.to_datetime(data["timestamp"], unit="ms")

    if len(df) > 0:
        temp_df = pd.DataFrame(data)
        df = df.append(temp_df)

    else:
        df = data

    if save:
        try:
            df.to_csv(filename, index=False)
        except FileNotFoundError:
            pass

    print("Update completed")

    return df


# %%
def plot_candlesticks(df, mav):
    """
    Plots candlesticks
    """
    # Set up some axes
    f1, ax = plt.subplots(figsize=(10, 5))

    # Create the plot and format the x axis
    mpf.plot(ax=ax, opens=df["open"], highs=df["high"], lows=df["low"], closes=df["close"],
             width=0.6, colorup="green", colordown="red", type="candle", mav=mav)

    plt.show()