"""
Script for creating a PyTorch dataset
"""
#%% Setup
import numpy as np
import datatable as dt
import torch

from datatable import f, fread, by, join
from torch.utils.data import Dataset

import talib

from _constants import *

# TODO: Try a Tensor-based approach to slice all pairs simultaneously (i.e. stack a timeframe across all pairs)

#%% Dataset
class CryptoDataset(Dataset):
    def __init__(self, timeframe, horizon_backward, horizon_forward, aux_timeframe_mapping):
        self.timeframe = timeframe
        self.horizon_backward = horizon_backward
        self.horizon_forward = horizon_forward

        self.aux_timeframe_mapping = aux_timeframe_mapping # Determines the number of steps to pull for the various timeframes

        # Get the earliest timestamp common to all pairs
        self.start_time = int(fread(f"data/start_dates.jay")[:, dt.max(f["Start Time"])].to_numpy()[0][0])

        # Determine which timeframes to load
        timeframes = [timeframe] + [timeframe for timeframe in aux_timeframe_mapping]

        # Load the datasets
        self.datasets = {}
        self.timestamps = {}
        rows = []
        for kline in timeframes:
            pairs_data = []
            for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
                # Load the dataset
                dt_ = fread(f"data/{symbol}-{kline}-data.jay")

                # Indicators
                # RSI
                dt_ = dt.cbind([dt_, dt.Frame({"RSI": talib.RSI(dt_["Close"].to_numpy().squeeze()) / 100})])

                # Stochastic RSI
                dt_ = dt.cbind([dt_, dt.Frame({ind:x / 100 for ind,x in
                                               zip(["StochRSI_A", "StochRSI_B"],
                                                   talib.STOCHRSI(dt_["Close"].to_numpy().squeeze()))})])

                # Filter to the start time
                dt_ = dt_[f.Timestamp >= self.start_time,:]

                # Aggregate any duplicate timestamps
                pairs_data.append(dt_[:, (dt.mean(f.Open), dt.mean(f.High), dt.mean(f.Low), dt.mean(f.Close),
                                          dt.mean(f.Volume), dt.mean(f.RSI), dt.mean(f.StochRSI_A), dt.mean(f.StochRSI_B)),
                                  by(f.Timestamp)])

                # Materialize and append to storage
                pairs_data[-1].materialize(to_memory=True)

                # Store the valid rows of the table
                rows.append(pairs_data[-1][:, {"Timestamp":f.Timestamp, "Valid":1}])

            # Get the smallest valid Timestamps
            times = rows[0]
            for i,dt_ in enumerate(rows[1:]):
                dt_.key = "Timestamp"
                times.key = "Timestamp"
                times = times[:,:, join(dt_)]
                times = times[(f[f"Valid.{i}"] == 1) & (f[f"Valid"] == 1), :]
            times = times[:, (f.Timestamp, f.Valid)]
            times.key = "Timestamp"

            # Filter all tables
            for i,dt_ in enumerate(pairs_data):
                pairs_data[i].key = "Timestamp"
                pairs_data[i] = pairs_data[i][:, :, join(times)]
                pairs_data[i] = pairs_data[i][f.Valid == 1, :]

            # Filter and stack
            self.timestamps[kline] = pairs_data[0].to_numpy()[:,0]
            self.datasets[kline] = torch.Tensor(np.stack([x.to_numpy()[:, 1:-1] for i,x in enumerate(pairs_data)], axis=1)).flatten(start_dim=1)

        # Determine the amount of available data
        self.data_length = self.datasets[self.timeframe].shape[0]

        # Get the indices of price
        self.indices = self._get_price_indices(self.datasets[self.timeframe].shape[1])


    def __len__(self):
        return self.data_length - self.horizon_backward - self.horizon_forward - 500

    def __getitem__(self, i):
        i = i + 500
        # Get the main timeframe data
        X = {self.timeframe:self.datasets[self.timeframe][i:i + self.horizon_backward, :]}

        # Determine the last timestamp
        last_time = self.timestamps[self.timeframe][i:i + self.horizon_backward][-1]

        # Slice the auxiliary timeframes
        for tf, history in self.datasets.items():
            if tf != self.timeframe:
                X[tf] = history[self.timestamps[tf] <= last_time][-self.aux_timeframe_mapping[tf]:]

        # Min-max scaling of price
        extrema = {tf:(X_.min(dim=0, )[0], X_.max(dim=0)[0]) for tf, X_ in X.items()}

        # Scale the non-indicators
        for tf, X_ in X.items():
            X_[:, self.indices] = torch.clamp((X_[:, self.indices] - extrema[tf][0][self.indices]) / (extrema[tf][1][self.indices] - extrema[tf][0][self.indices] + 0.00001), 0, 1.0)

        y = self.datasets[self.timeframe][i + self.horizon_backward:i + self.horizon_backward + self.horizon_forward, 0]

        # Scale y
        #y = (y - extrema[tf][0][0]) / (extrema[tf][1][0] - extrema[tf][0][0])

        # Transform y
        y = torch.from_numpy(np.array((np.polyfit([x for x in range(y.shape[0])], y, 1)[0] > 0).astype(np.float32)))

        return X,y

    def _get_price_indices(self, feature_length):
        # Indices
        indices = []
        j = 0
        k = 0
        for i in range(feature_length):
            if j < 5:
                indices.append(i)
                j += 1

            if j >= 5 and k <= 3:
                k += 1

            if k > 3:
                k = 0
                j = 0

        return indices



