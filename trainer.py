"""
Debugging script
"""
#%% Setup
from tqdm import tqdm
from datasetV2 import CryptoDataset
from torch.utils.data import DataLoader
from model import Multi_DA_RNN
from torch.nn import MSELoss, BCEWithLogitsLoss
from torch.optim import Adam

import pytorch_lightning as pl

#%% TODO
'''
1. Test different targets (e.g. increase or decreasing gradient, +5% change, exact prices
2. Upfront convolutional/fully connected layers to reduce feature space going in to DA RNN
'''

#%% Dataset
ds = CryptoDataset(horizon_backward=360, horizon_forward=12, timeframe="5m",
                   aux_timeframe_mapping={"1m":60, "15m":200, "1h":60, "6h":6})

loader = DataLoader(ds, batch_size=8, num_workers=0, shuffle=True)

# Data sizes
sizes = {tf:list(X.shape) for tf,X in ds.__getitem__(0)[0].items()}

#%% Model
config_dict = {"5m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 360,
                     "input_shape": sizes["5m"], "output_shape": 128},
               "1m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 60,
                     "input_shape": sizes["1m"], "output_shape": 128},
               "15m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 200,
                     "input_shape": sizes["15m"], "output_shape": 128},
               "1h":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 60,
                     "input_shape": sizes["1h"], "output_shape": 128},
               "6h":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 6,
                     "input_shape": sizes["6h"], "output_shape": 128}}

net = Multi_DA_RNN(config_dict, da_rnn_output_dims=100, output_shape=1, hidden_dims=1000)

#%% Lightning Model
class CryptoNet(pl.LightningModule):
    def __init__(self):
        super(CryptoNet, self).__init__()

        # Model
        self.net = net
        self.net.cuda()

        # Loss function
        self.loss = BCEWithLogitsLoss()

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=0.02)

    def forward(self, x):
        return model(x)

    def training_step(self, batch, batch_nb):
        # Unpack the batch
        X, y = batch

        # Forward pass
        y_ = self.net(X)

        # Calculate loss
        loss = self.loss(y_.squeeze(), y)

        return {"loss": loss}

#%% Training
model = CryptoNet()
trainer = pl.Trainer(gpus=1, accumulate_grad_batches=8)
trainer.fit(model, train_dataloader=loader)

