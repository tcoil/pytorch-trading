"""
Testing of plotting and indicators
"""
#%% Setup
import pandas as pd

from cryptotools import plot_candlesticks
import talib

import mplfinance as mpf
import matplotlib.pyplot as plt

#%% Data import
btcusdt = pd.read_csv("data/BTCUSDT-1d-data.csv")
btcusdt.loc[:, "timestamp"] = pd.DatetimeIndex(btcusdt["timestamp"])
btcusdt = btcusdt.set_index("timestamp").rename(columns={"open": "Open", "high": "High", "low": "Low", "close": "Close"})

#%%
rsi = talib.RSI(btcusdt["Close"])
stoch_rsi = talib.STOCHRSI(btcusdt["Close"])

#%% Plot RSI and StochRSI
plt.plot(rsi[-100:])
plt.plot(stoch_rsi[0][-100:])
plt.plot(stoch_rsi[1][-100:])
plt.show()

#%% Plotting
fig, ax = mpf.plot(btcusdt, type="candle", mav=(10,20,50,100,200), style="mike", returnfig=True)
plt.show()