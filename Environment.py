"""
Class for handling the environment of the model
"""
#%% Setup
import numpy  as np
import pandas as pd

#%% Environment class
class Environment:
    def __init__( self
                , dataset         :pd.DataFrame
                , forward_horizon :int
                , backward_horizon:int
                , inflation       :float = 0.001
                , transaction_fee :float = 0.005
                ):
        """
        An environment for training trading bots
        :param dataset:
        :param forward_horizon : The number of time steps that the environment will step through
        :param backward_horizon: The number of time steps that the model will be able to look backwards on
        :param inflation       : An inflation rate, which acts as a living penalty
        """
        # Class attributes
        self.dataset          = dataset
        self.forward_horizon  = forward_horizon
        self.backward_horizon = backward_horizon

        # Initialize placeholder attributes
        self.active_data = None
        self.results     = None

        # Create the indices
        self.indices = np.arange(backward_horizon, dataset.shape[0] - forward_horizon)

    def step(self):
        """
        Step the environment forward by 1 step
        :return:
        """
        # Increment the index
        self.active_index += 1

        # Fetch the historical data
        self.active_data  = self.dataset.iloc[(self.active_index - self.backward_horizon):self.active_index]

        self.calculate_return()

    def reset(self):
        """
        Reset the environment, choosing a random time point
        :return:
        """
        # Choose an appropriate index
        self.active_index = np.random.choice(self.indices)

        # Fetch the associated data

        self.active_index = self.backward_horizon

        # Instantiate the results table
        self.results = self.dataset.iloc[self.active_index]

        # Add the appropriate columns
        self.results["actual_fiat_balance"]    = 100
        self.results["effective_fiat_balance"] = 100
        self.results["actual_coin_balance"]    = 0
        self.results["effective_coin_balance"] = self.results.eval("actual_fiat_balance / ((opening + closing) / 2)")

        self.results["roi"] = 0


    def buy(self, amount):
        """
        The amount of each coin to purchase, relative to the available fiat balance
        :param amounts:
        :return:
        """
        #TODO: check that amount can be sold

        # Get the current fiat balance
        fiat_balance = self.results["fiat_balance"].iloc[-1]

        # Calculate the fiat balance
        new_fiat_balance = fiat_balance * (1 - amount) * (1 - self.transaction_fee)

        # Calculate the new coin balance
        new_coin_balance = fiat_balance \
                         * amount \
                         * (1 / self.results.eval("actual_fiat_balance / ((opening + closing) / 2)")) \
                         * (1 - self.transaction_fee)

        # Update actual balances
        self.results.iloc[-1]["actual_fiat_balance"] = new_fiat_balance
        self.results.iloc[-1]["actual_coin_balance"] = new_coin_balance

        # Update effective balances

    def sell(self, amount):
        """
        The amount of each coin to sell, relative to the available coin balance
        :param amounts:
        :return:
        """
        # TODO: check that amount can be sold

    def calculate_return(self):
        """
        Calculates the current ROI relative to the baseline
        :return:
        """
        roi = self.results["effective_fiat_balance"][-1] / self.results["effective_fiat_balance"][0]

        return roi
