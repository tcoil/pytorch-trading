"""
Debugging script
"""
#%% Setup
from tqdm import tqdm
from datasetV2 import CryptoDataset
from torch.utils.data import DataLoader

from model import Multi_DA_RNN

#%% Dataset
ds = CryptoDataset(horizon_backward=360, horizon_forward=12, timeframe="5m",
                   aux_timeframe_mapping={"1m":60, "15m":200, "1h":60, "6h":6})

loader = DataLoader(ds, batch_size=8, num_workers=0)

# Data sizes
sizes = {tf:list(X.shape) for tf,X in ds.__getitem__(0)[0].items()}

#%% Model
config_dict = {"5m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 360,
                     "input_shape": sizes["5m"], "output_shape": 128},
               "1m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 60,
                     "input_shape": sizes["1m"], "output_shape": 128},
               "15m":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 200,
                     "input_shape": sizes["15m"], "output_shape": 128},
               "1h":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 60,
                     "input_shape": sizes["1h"], "output_shape": 128},
               "6h":{"encoder_num_hidden": 128, "decoder_num_hidden": 128, "horizon_backward": 6,
                     "input_shape": sizes["6h"], "output_shape": 128}}

model = Multi_DA_RNN(config_dict, da_rnn_output_dims=100, horizon_forward=12, hidden_dims=1000)

#%%
for sample in tqdm(loader):
    y_ = model(sample[0])

a=0
