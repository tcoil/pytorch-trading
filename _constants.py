"""
Script for storing constants for use between different scripts
"""
# Pairs
USDT_PAIRS = ["BTC"]
BTC_PAIRS = ["POLY", "HBAR", "XTZ", "XEM", "ETH", "ADA", "LINK", "ATOM", "LTC", "XRP", "MATIC", "XLM", "XMR", "WRX",
             "ETC", "KNC", "BAT", "RVN", "STEEM", "DASH", "ZIL", "IOTA", "QKC", "OMG", "BCH", "BNB", "EOS", "ICX"]

# Timeframes
TIMEFRAMES = ["1m", "5m", "15m", "1h", "6h", "1d"]